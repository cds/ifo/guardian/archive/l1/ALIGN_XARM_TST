#!/usr/bin/env python
"""
Rewrite of align_save.py to be:
 > More Pythonic.
 > Importable as a function, which is better for guardian applications.

Should be maintained to be compatible with guardian. Funbdamentally this means 
that the script should be:
 > Able to be imported.
 > Continue to have the sole __all__ element \"align_save\" performing the 
   function of this script for a single optic.
 > Not modify the path, guardian doesn't like this.
If in doubt ask.

Copied across from /opt/rtcds/userapps/trunk/sus/common/scripts/align_save.py 
which was written by S. Aston in 2018.

Author: N. Holland.
Date (yyyy-mm-dd): 2019-05-02
Contact: nathan.holland@ligo.org

Modified: 2019-05-02 (Created)
Modified: 2019-05-03 (Spelling corrections)
Modified: 2019-05-21 (Minor debugging)
Modified: 2019-05-22 (Minor debugging)
Modified: 2019-05-23 (Debugging on test stand X2)
"""
#-------------------------------------------------------------------------------
#Imports:
import argparse;
import ezca;
import os;
import subprocess;
import time;
#
#-------------------------------------------------------------------------------
#EPICS configuration:
axs = ezca.Ezca();
    #EPICS channel access.
#
#-------------------------------------------------------------------------------
#Environment configuration (INCOMPATIBLE WITH GUARDIAN):
#import sys;
    #Allow the path to be changed (incompatible with guardian).
#sys.path.append('/ligo/cdscfg');
    #Find the location of /ligo/cdscfg/stdenv.py
#import stdenv as cds;
    #Import the standard CDS environment files.
#cds.INIT_ENV();
    #Initialise the standard CDS environment for the OS, and site.
#_userapps_dir = cds.USERAPPS;
    #Get the correct userapps directory.
#_ifo = cds.ifo;
    #Get the correct, lower case, IFO string.
#
#Alternative (LESS IDEAL):
if os.getenv("ifo") is not None:
    #If the environment variable 'ifo' is defined.
    _ifo = os.getenv("ifo").lower();
        #Lowered just to make sure.
elif os.getenv("IFO") is not None:
    #1st backup, if 'IFO' is defined.
    _ifo = os.getenv("IFO").lower();
else:
    #Hard, site specific, backup.
    _ifo = "l1";
#
if os.getenv("PRIVATE_USERAPPS") is not None:
    #Copied from /ligo/cdscfg/llo/stddir.py on 2019-05-22.
    #If there is a defined userapps directory.
    _userapps_dir = os.getenv("PRIVATE_USERAPPS");
else:
    #Copied from /ligo/cdscfg/llo/stddir.py on 2019-05-22.
    #Hard coded backup.
    _userapps_dir = os.path.join(os.path.sep, 'opt', 'rtcds', 'userapps',
                                 'release');
#
#-------------------------------------------------------------------------------
#Script Variables:
__all__ = ["align_save"];
    #The functional version of this script.
#-------------------------------------------------------------------------------
#Functions:
def optic_details(optic):
    """
    Gets the filename, path and partial channel name for a specified optic.
    Hardcoded based upon the file this was copied from, dated 2018-05-10.
    
    Usage:
    path, filename, part_chan_name = optic_details(optic);
    path           - The path to the file storing the offset values.
    filename       - The filename storing the offset values.
    part_chan_name - A partial channel name, up to numbered mass.
    optic          - The name of the optic.
    """
    #
    namebit2 = optic[:2];
        #First 2 characters of the name.
    namebit3 = optic[:3];
        #First 3 characters of the name.
    #
    if namebit2 == "IM":
        pathname = _userapps_dir + '/sus/' + _ifo + '/burtfiles/im/';
            #Set path name - appropriately.
            #Copied.
    elif (namebit2 == "RM") or (namebit2 == "OM") or (namebit2 == "ZM"):
        pathname = _userapps_dir + '/sus/' + _ifo + '/burtfiles/htts/';
            #Set path name - appropriately.
            #Copied.
    else:
        pathname = _userapps_dir + '/sus/' + _ifo + '/burtfiles/' + \
                   optic.lower() + '/';
            #Set path name - appropriately.
            #Copied.
    if namebit3 == "OMC":
        pathname = _userapps_dir + '/sus/' + _ifo + '/burtfiles/' + \
                   optic.lower() + '/';
            #Set path name - appropriately.
            #Copied.
    #
    filename = optic + '_alignment_values.txt';
        #The filename to append the offset to.
    #
    comm_name = "SUS-" + optic;
        #The common part of the name.
    if (namebit3 == "ITM") or (namebit3 == "ETM"):
        part_chan_name = comm_name + '_M0_';
            #Use M0 for the TMs.
    else:
        part_chan_name = comm_name + '_M1_';
            #Use M1 for all other masses.
    #
    return(pathname, filename, part_chan_name);
#
def optic_deflection(part_chan_name):
    """
    Get the yaw, and usually pitch too (except in the case of OFI optic/s) of 
    the optic.
    
    Usage:
    offset_tuple = optic_deflection(part_chan_name)
    """
    namebit3 = part_chan_name[4:7];
    #
    yaw_chnl = "OPTICALIGN_Y_OFFSET";
        #Name for yaw offset.
    pit_chnl = "OPTICALIGN_P_OFFSET";
        #Name for pitch offset.
    #
    if namebit3 == "OFI":
        #Case handling for OFI optics, which don't have pitch offset.
        yaw = axs.read(part_chan_name + yaw_chnl);
            #Read the yaw offset.
        return(yaw,)
            #Return as a tuple.
    else:
        pitch = axs.read(part_chan_name + pit_chnl);
            #Read the pitch offset.
        yaw = axs.read(part_chan_name + yaw_chnl);
            #Read the yaw offset.
        return(pitch, yaw);
    #
#
def write_to_file(path, filename, values_tuple):
    """
    Append the offset/s to file.
    
    Usage:
    write_to_file(path, filename, values_tuple)
    path         - Path of the file to append to.
    filename     - Name of the file to append to.
    values_tuple - Values tuple of the offsets, as returned by optic_deflection.
    """
    string_to_write = time.strftime("%a %b %d %H:%M:%S %Y");
        #The date/time string, as copied across (without trailing space).
    #
    offsets = map(format, values_tuple, ["0.2f" for ii in values_tuple]);
        #Map as a string, same mapping as original.
    #
    for offset in offsets:
        string_to_write += ' ';
            #Add a delimiting space.
        string_to_write += offset;
            #Add the offset, in order.
    #
    string_to_write += '\n';
        #Add carrige return to string.
    #
    with open(path + filename, "a") as fl:
        fl.write(string_to_write);
            #Write to file.
    #
#
#-------------------------------------------------------------------------------
#MAIN:
def align_save(optic):
    """
    Saves the yaw, and often pitch, offsets of the provided optic to file. Only 
    yaw is available for the OFI. The current value/s is appended to the file 
    along with the date and time of the save. Order is:
    <day> <month> <date> <hour>:<minute>:<second> <year> <pitch> <yaw>
    
    Usage:
    align_save(optic)
    optic - The optic to save the alignment for.
    """
    path, filename, part_chan_name = optic_details(optic);
        #Get the path, filename, and partial EPICS channel name.
    offset_tuple = optic_deflection(part_chan_name);
        #Get the offset tuple, one of:
        #(yaw,) for OFI optics.
        #(pitch, yaw) for all other optics.
    write_to_file(path, filename, offset_tuple);
        #Write the offset/s to file.
#
#-------------------------------------------------------------------------------
#Argparse:
_prog_descr = "Saves the current offset, pitch and yaw (generally) on the ";
_prog_descr += "requested optic. Only yaw can be saved on the OFI.";
    #The description of this program, briefly.
parser = argparse.ArgumentParser(description = _prog_descr);
    #The argument parser, with a program description.
#
optic_help = "Please provide one, or more optic names eg. ETMY, SR3.";
    #The description of the optic argument.
parser.add_argument("optic", nargs = '+', type = str, help = optic_help);
    #Add the optic argument, all arguments are collected into a list.
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parser.parse_args();
            #Parse the arguments given.
    #
    for optik in args.optic:
        align_save(optik);
            #Save all of the optics provided.
    #
else:
    pass;
#
#-------------------------------------------------------------------------------
#END.

