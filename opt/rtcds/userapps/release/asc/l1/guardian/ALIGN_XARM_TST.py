#Docstring:
"""
Inital Alignment Guardian: Green X Arm Cavity Alignment - Test.

A test to assis with determining the problem with the original ALIGN_ARM, and 
ALIGN_XARM scripts. Specifically this script is to see if the imports are 
causing the node management, of the ALS_XARM, guardian to fail.

Authors: N. A. Holland, A. Mullavey.
Date: 2019-06-03
Contact: nathan.holland@ligo.org

Modified: 2019-06-03 (Created; copied across from ALIGN_ARM and ALIGN_XARM.).
"""
#-------------------------------------------------------------------------------
#Imports:
import numpy as np;
#
from optic import Optic;
    #Import the class Optic from 
    #/opt/rtcds/userapps/release/asc/l1/guardian/optic.py
#
from guardian import GuardState, Node;
    #Import the minimum required number of guardian subutilities.
#
import isclib.matrices as matrix;
    #Import the LSC matrices access. Source is located at:
    #/opt/rtcds/userapps/release/isc/l1/guardian
#
from align_dither import DigitalDemodulation, AlignmentDither;
    #Import the classes DigitalDemodulation, and AlignmentDither from
    #/opt/rtcds/userapps/release/asc/l1/scripts/align_dither.py
#
from new_align_restore import align_restore;
    #Import the function align_restore from 
    #/opt/rtcds/userapps/release/sus/common/scripts/align_restore.py
from new_align_save import align_save;
    #Import the function align_save from 
    #/opt/rtcds/userapps/release/sus/common/scripts/align_save.py
#-------------------------------------------------------------------------------
#Script variables:
nominal = "ARM_ALIGN_IDLE";
#
arm = 'X';
    #The arm being aligned.
ITM = "ITMX";
    #The input test mass being aligned.
ETM = "ETMX";
    #The end test mass being aligned.
TMS = "TMSX";
    #The TMS bench being aligned.
itm = Optic("ITMX");
    #Conveninet access for the ITM.
etm = Optic("ETMX");
    #Convenient access for the ETM.
tms = Optic("TMSX");
    #Convenient access for the TMS bench.
#als_loose = Node("ALS_XARM");
    #The ALS arm guardian to manage.
## General values for TMS alignment:
tms_oscillator_number = "9";
    #The oscillator number for the TMS alignment.
tms_pit = -5300;
tms_yaw = 1695;
    #The optimised alignment values for the TMS bench.
    #Ensures that the TMS bench approximately points to the ITM baffle PD.
tms_pwr_min = 0.87;
    #The minimum power level needed to run the TMS alignment stage.
tms_G_pit = -0.8;
    #The gain for the TMS alignment dithering in pitch.
tms_G_yaw = -0.3;
    #The gain for the TMS alignment dithering in yaw.
#
## General values for the ITM alignment.
itm_pit_ofs = -360.0;
itm_yaw_ofs = -850.0;
    #The optimised alignment values for the ITM.
    #Need to be adjusted by the gain of the filter module.
    #Ensures that ITM approximately points to the ETM baffle PD.
itm_pwr_min = 0.4;
    #The minimum power needed to run the ITM alignment step.
itm_G_pit = 0.5;
    #The gain for the ITM alignment dithering in pitch.
itm_G_yaw = -0.5;
    #The gain for the ITM alignment dithering in yaw.
#
## General values for the ETM alignment.
etm_pit_ofs = 0;
etm_yaw_ofs = 0;
    #The optimised alignment values for the ETM.
    #Need to be adjusted by the gain of the filter modules.
    #Ensures that ETM approximately points to the ITM baffle PD.
etm_pwr_min = 0;
    #The minimum power needed to run the ETM alignment step.
etm_G_pit = 0;
    #The gain for the ETM alignment dithering in pitch.
etm_G_yaw = 0;
    #The gain for the ETM alignment dithering in yaw.
#-------------------------------------------------------------------------------
#Functions:
#
#-------------------------------------------------------------------------------
#Classes:
#
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#                                 Guardian States
#-------------------------------------------------------------------------------
#
class INIT(GuardState):
    #
    
    request = False; #Can't request the initial state.
    #index = ; 
    
    def main(self):
        #Does nothing.
        #
        return(True);
    #

#
#-------------------------------------------------------------------------------
#Does nothing
class ARM_ALIGN_IDLE(GuardState):
    #Does nothing but wait.
    
    goto = True; #Individual arm safe reset location.
    request = True; #You can request a rest of each arm's optics.
    #index = ;
    
    def main(self):
        #Releases the ALS arm guardian.
        pass;
        #als_loose.release();
            #Releases the ALS arm guardian from control by this guardian.
    #
    
    def run(self):
        #Is successful.
        return(True);
    #
    
#
#-------------------------------------------------------------------------------
#
class ARM_ALIGN_RESET(GuardState):
    #Resets arm specific optics.
    
    request = True; #You can request a rest of each arm's optics.
    #index = ;
    
    def main(self):
        ## Reset masses:
        for dof in ['P', 'Y']:
            #Remove offsets from the optics.
            itm.align(dof, ezca);
            etm.align(dof, ezca);
            tms.align(dof, ezca);
        #
        align_restore(ITM);
        align_restore(ETM);
        align_restore(TMS);
            #Restore the saved values for the optics.
        #
        ## Turn off oscillators:
        ezca.write(":ASC-ADS_PIT{0}_OSC_FREQ".format(tms_oscillator_number), 
                   0);
        ezca.write(":ASC-ADS_PIT{0}_OSC_CLKGAIN".format(tms_oscillator_number),
                   0);
        ezca.write(":ASC-ADS_LO_PIT_MTRX_{0}_{0}".format(tms_oscillator_number),
                   0);
            #Disable the TMS Pitch oscillator.
        #
        ezca.write(":ASC-ADS_YAW{0}_OSC_FREQ".format(tms_oscillator_number), 
                   0);
        ezca.write(":ASC-ADS_YAW{0}_OSC_CLKGAIN".format(tms_oscillator_number),
                   0);
        ezca.write(":ASC-ADS_LO_YAW_MTRX_{0}_{0}".format(tms_oscillator_number),
                   0);
            #Disable the TMS Yaw oscillator.
        #
        ## Switch off optic test filters:
        ezca.switch(":SUS-TMS{0}_M1_TEST_P".format(arm), "OUTPUT", "OFF");
        ezca.switch(":SUS-TMS{0}_M1_TEST_Y".format(arm), "OUTPUT", "OFF");
            #Turn off TMS filters.
        ezca.switch(":SUS-ITM{0}_M0_TEST_P".format(arm), "OUTPUT", "OFF");
        ezca.switch(":SUS-ITM{0}_M0_TEST_Y".format(arm), "OUTPUT", "OFF");
            #Turn off ITMX filters.
        #
        #als_loose.set_managed();
            #Set the ALS guardian to be managed by this guardian.
        ## Start a Timer:
        self.timer["align_reset"] = 5.0;
            #5 second timer to ensure that the changes are implemented.
    #
    
    def run(self):
        #Wait for timer to expire before moving on.
        if self.timer["align_reset"]:
            return(True);
        #
    #
    
#
#-------------------------------------------------------------------------------
#Misalign ETM to prevent green cavity from being resonant.
class MISALIGN_FOR_GRN_CAV(GuardState):
    #Misalign optics that will hinder locking the green arm cavities.
    
    request = True; #Can request this state.
    #index = ;
    
    def main(self):
        #Misalign optics so that the green arms aren't resonant.
        log("Misaligning {0}.".format(ETM));
            #Provide information for the operator.
        etm.misalign('P', -40, ezca);
        #etm_P_G = ezca.read(":SUS-ETM{0}_M0_OPTICALIGN_P_GAIN".format(arm));
        #etm_Y_G = ezca.read(":SUS-ETM{0}_M0_OPTICALIGN_Y_GAIN".format(arm));
            #The gain values for adjustment of the offsets.
        #etm.misalign('P', -etm_pit_ofs / etm_P_G, ezca);
        #etm.misalign('Y', -etm_yaw_ofs / etm_Y_G, ezca);
            #Misalign the ETM so the GRN CAV isn't resonant.
            #TODO - Point ETM where there ISN'T a baffle PD.
        #
        self.timer["wait"] = 1.0;
            #A timer to wait 
    #
    
    def run(self):
        #Pause.
        if self.timer["wait"]:
            return(True);
                #Once the timer expires move on to the next state.
        #
    #
#
#-------------------------------------------------------------------------------
#
class ALIGN_GRN_TO_TMS(GuardState):
    #Ensure that the TMS bench is aligned to the green beams.
    #Done by the ALS Guardian
    
    request = False; #This is a transition state.
    #index = ;
    
    def main(self):
        #Alignment procedure, uses the ALS guardian.
        #als_loose.set_request("QPDS_LOCKED");
            #Request that the ALS guardian lock the GRN beam to the TMS.
        #
        self.timer["check"] = 2.0;
            #A timer to check if/when the 
        self.timer["max_wait"] = 60.0;
            #A timeout timer for the ALS guardian.
    #
    
    def run(self):
        #Check every so often that the ALS guardian has locked the green.
        if self.timer["check"]:
            return True
            
            #The timer has expired.
            #if als_loose.arrived:
                #return(True);
                    #The ALS guardian has arrived in the QPDS_LOCKED state.
            #else:
                #self.timer["check"] = 2.0;
                    #Reset the timer.
            #
        #
        if self.timer["max_wait"]:
            log("ALS_{0}ARM guardian failed to reach ".format(arm) + \
                "\"QPDS_LOCKED\" within 60 seconds (timeout).");
                #Inform the operator of what is going on.
            #
            return("ARM_ALIGN_IDLE");
                #Restart the alignment process, current iteration has failed.
        #
    #

#
#-------------------------------------------------------------------------------
#
class GRN_ALIGNED_TO_TMS(GuardState):
    #Ensure the TMS bench is aligned to the green beams.
    
    request = True; #Can request that the green is aligned to the TMS.
    #index = ;
    
    def run(self):
        return True
        
        #Periodically check that the TMS is aligned to the green beam.
        #if not als_loose.completed:
            #The QPDS_LOCKED state is still running, wait for it.
            #return(False);
                #We are not ready to procede.
        #Perform other checks, for example stalled states.
        #elif als_loose.state != "QPDS_LOCKED":
            #The state is not what it should be.
            #log("ALS_{0}ARM guardian is not in \"QPDS_LOCKED\".".format(arm));
                #Warn the operator that the state is not matched to the intended
                #state.
            #
            #return("ARM_ALIGN_IDLE");
                #Restart alignment.
        #else:
            #return(True)
    #
#
#-------------------------------------------------------------------------------
#
class ALIGN_TMS_TO_ITM_BFFL(GuardState):
    #Align the TMS, green, to the PD on the ITM baffle.
    
    request = False; #This is a transition state.
    #index = ;
    
    def main(self):
        #Alignment procedure.
        log("Aligning TMS{0} to ITM{0} Baffle PD 1.".format(arm));
            #Provide information to the operator about what is happening.
        #
        ezca.ramp_offset(":SUS-TMS{0}_M1_TEST_P".format(arm), tms_pit, 10,
                         wait = False);
        ezca.ramp_offset(":SUS-TMS{0}_M1_TEST_Y".format(arm), tms_yaw, 10);
            #Ramp the offsets to pre-saved values over 10 seconds.
            #Will this work, it is a method of ezca.ligofilter.LIGOFilter.
        #
        ezca.switch(":SUS-TMS{0}_M1_TEST_P".format(arm), "OFFSET", "OUTPUT",
                    "ON");
        ezca.switch(":SUS-TMS{0}_M1_TEST_Y".format(arm), "OFFSET", "OUTPUT",
                    "ON");
            #Turn the offsets and output on.
        #
        tms.optic_align_ramp('P', 1, ezca);
        tms.optic_align_ramp('Y', 1, ezca);
        tms.test_ramp('P', 10, ezca);
        tms.test_ramp('Y', 10, ezca);
            #Set the ramp times, as in the dither script.
        #
        ezca.write(":ASC-ADS_PIT{0}_OSC_FREQ".format(tms_oscillator_number), 
                   2.8);
        ezca.write(":ASC-ADS_PIT{0}_OSC_CLKGAIN".format(tms_oscillator_number),
                   200);
        ezca.write(":ASC-ADS_LO_PIT_MTRX_{0}_{0}".format(tms_oscillator_number),
                   1);
            #Enable the TMS Pitch oscillator at 2.8 Hz.
        #
        ezca.write(":ASC-ADS_YAW{0}_OSC_FREQ".format(tms_oscillator_number), 
                   3.7);
        ezca.write(":ASC-ADS_YAW{0}_OSC_CLKGAIN".format(tms_oscillator_number),
                   200);
        ezca.write(":ASC-ADS_LO_YAW_MTRX_{0}_{0}".format(tms_oscillator_number),
                   1);
            #Enable the TMS Yaw oscillator at 3.7 Hz.
        #
        self.dig_demod = DigitalDemodulation(ezca,
            ":AOS-ITM{0}_BAFFLEPD_1_POWER".format(arm),
            ":SUS-TMS{0}_M1_LOCK_P_INMON".format(arm),
            ":SUS-TMS{0}_M1_LOCK_Y_INMON".format(arm));
            #The digital demodulation object.
        #
        self.p_ad = AlignmentDither(ezca, 
            ":SUS-TMS{0}_M1_OPTICALIGN_P_OFFSET".format(arm),
            ":SUS-TMS{0}_M1_TEST_L_OFFSET".format(arm), tms_G_pit);
            #The alignment dither object for Pitch.
        self.y_ad = AlignmentDither(ezca, 
            ":SUS-TMS{0}_M1_OPTICALIGN_Y_OFFSET".format(arm),
            ":SUS-TMS{0}_M1_TEST_T_OFFSET".format(arm), tms_G_yaw);
            #The alignment dither object for Yaw.
        #
        self.timer["max_wait"] = 60.0;
            #Try to align for up to 60 seconds.
            #TODO: In future use this timer as a maximum iteration time.
            #      After this maximum time it will jump to a side state, and 
            #      prompt the operator to confirm the TMS alignment is good
            #      enough.
    #
    
    def run(self):
        #Run the alignment procedure.
        PD_pwr, Pit_demod, Yaw_demod = self.dig_demod.demod();
            #Collect the data, takes the default of 2 seconds.
        if PD_pwr < tms_pwr_min:
            log("Green power on ITM{0} Baffle PD 1, too low.".format(arm));
                #Send information up about the failure.
            #
            return("FIND_ITM_BFFL_PD_WITH_TMS");
                #Jump to TMS scan state.
        #
        self.p_ad.align_dither(Pit_demod, PD_pwr);
        self.y_ad.align_dither(Yaw_demod, PD_pwr);
            #Do one iteration of the pitch and yaw alignment dithering.
        #
        if self.timer["max_wait"]:
            return(True);
                #After 60 seconds move on.
                #TODO: In future use the error signal to determine the final
                #      point to stop.
        #
    #
#
#-------------------------------------------------------------------------------
#
class FIND_ITM_BFFL_PD_WITH_TMS(GuardState):
    #Currnelt a request for the operator to manually align the TMS
    
    request = False; #Not requestable.
    #index = ;
    
    def main(self):
        #
        self.dig_demod = DigitalDemodulation(ezca,
            ":AOS-ITM{0}_BAFFLEPD_1_POWER".format(arm),
            ":SUS-TMS{0}_M1_LOCK_P_INMON".format(arm),
            ":SUS-TMS{0}_M1_LOCK_Y_INMON".format(arm));
            #The digital demodulation object.
        #
        #Do I need to turn off the oscillations here?
        #
        self.timer["remind"] = 15.0;
            #Set up a timer for reminding the operator to align the TMS.
        #
    #
    
    def run(self):
        #
        PD_pwr, _ = self.dig_demod.demod();
            #Collect the data, takes the default of 2 seconds.
        #
        if self.timer["remind"]:
            #Make sure the operator knows what is going on.
            log("Green power on ITM{0} Baffle PD 1, too low. ".format(arm) + \
                "Please manually align TMS{0} to ITM{0} ".format(arm) + \
                "baffle 1 PD.");
                #Remind the operator.
            #
            self.timer["remind"] = 15.0;
                #Reset the timer.
        #
        if PD_pwr >= tms_pwr_min:
            #Power level IS now sufficient.
            log("Green power on ITM{0} Baffle PD 1 is sufficient.".format(arm));
            #
            return("ALIGN_TMS_TO_ITM_BFFL");
                #Jump back to the automatic alignment step.
                #Would a return(True) be sufficient here? Perhaps.
        #
        return(False);
            #Keep looping.
    #

#
#-------------------------------------------------------------------------------
#
class TMS_ALIGNED_TO_ITM_BFFL(GuardState):
    #Ensure the TMS GRN is aligned to the ITM baffle PD.
    
    request = True; #Hey you may want it, for some reason.
    #index = ;
    
    def main(self):
        #Turn off the TMS alignment oscillator.
        ezca.write(":ASC-ADS_PIT{0}_OSC_FREQ".format(tms_oscillator_number), 
                   0);
        ezca.write(":ASC-ADS_PIT{0}_OSC_CLKGAIN".format(tms_oscillator_number),
                   0);
        ezca.write(":ASC-ADS_LO_PIT_MTRX_{0}_{0}".format(tms_oscillator_number),
                   0);
            #Disable the TMS Pitch oscillator.
        #
        ezca.write(":ASC-ADS_YAW{0}_OSC_FREQ".format(tms_oscillator_number), 
                   0);
        ezca.write(":ASC-ADS_YAW{0}_OSC_CLKGAIN".format(tms_oscillator_number),
                   0);
        ezca.write(":ASC-ADS_LO_YAW_MTRX_{0}_{0}".format(tms_oscillator_number),
                   0);
            #Disable the TMS Yaw oscillator.
        #
        align_save(TMS);
            #Save the offset of the TMS.
        #
        ezca.switch(":SUS-TMS{0}_M1_TEST_P".format(arm), "OUTPUT", "OFF");
        ezca.switch(":SUS-TMS{0}_M1_TEST_Y".format(arm), "OUTPUT", "OFF");
            #Turn the outputs off.
        #
        self.timer["wait"] = 2.0;
            #Set a timer.
    #
    
    def run(self):
        #Move along after the timer expires.
        if self.timer["wait"]:
            return(True);
        #
    #
#
#-------------------------------------------------------------------------------
#
class ALIGN_ITM_TO_ETM_BFFL(GuardState):
    #Align the ITM, green, to the ETM baffle PD.
    
    request = False; #This is a transition state.
    #index = ;
    
    def main(self):
        #Alignment procedure.
        log("Aligning ITM{0} to ETM{0} Baffle PD 1.".format(arm));
            #
        #
        ezca.write(":ASC-ADS_PIT{0}_OSC_FREQ".format(tms_oscillator_number), 
                   2.8);
        ezca.write(":ASC-ADS_PIT{0}_OSC_CLKGAIN".format(tms_oscillator_number),
                   200);
        ezca.write(":ASC-ADS_LO_PIT_MTRX_{0}_{0}".format(tms_oscillator_number),
                   1);
            #Enable the TMS Pitch oscillator at 2.8 Hz.
        #
        ezca.write(":ASC-ADS_YAW{0}_OSC_FREQ".format(tms_oscillator_number), 
                   3.7);
        ezca.write(":ASC-ADS_YAW{0}_OSC_CLKGAIN".format(tms_oscillator_number),
                   200);
        ezca.write(":ASC-ADS_LO_YAW_MTRX_{0}_{0}".format(tms_oscillator_number),
                   1);
            #Enable the TMS Yaw oscillator at 3.7 Hz.
        #
        itm_P_G = ezca.read(":SUS-ITM{0}_M0_OPTICALIGN_P_GAIN".format(arm));
        itm_Y_G = ezca.read(":SUS-ITM{0}_M0_OPTICALIGN_Y_GAIN".format(arm));
            #Gains for altering the offset to put on the itm.
        itm.test_gain('P', itm_P_G, ezca);
        itm.test_gain('Y', itm_Y_G, ezca);
            #Change the test filter gains.
        itm.test_ramp('P', 10, ezca);
        itm.optic_align_ramp('P', 1, ezca);
        itm.test_ramp('Y', 10, ezca);
        itm.optic_align_ramp('Y', 1, ezca);
            #Change the filter ramp times.
        itm.test_offset('P', itm_pit_ofs / itm_P_G, ezca);
        itm.test_offset('Y', itm_yaw_ofs / itm_Y_G, ezca);
            #Change the offset.
        ezca.switch(":SUS-ITM{0}_M0_TEST_P".format(arm), "OFFSET", "OUTPUT",
                    "ON");
        ezca.switch(":SUS-ITM{0}_M0_TEST_Y".format(arm), "OFFSET", "OUTPUT",
                    "ON");
            #Switch the filter and offset on.
            #TODO: Determine how much of the ramping is necessary.
            #      Ramp offsets, slowly, with ezca.ramp_offset
        #
        self.dig_demod = DigitalDemodulation(ezca,
            ":AOS-ETM{0}_BAFFLEPD_1_POWER".format(arm),
            ":SUS-TMS{0}_M1_LOCK_P_INMON".format(arm),
            ":SUS-TMS{0}_M1_LOCK_Y_INMON".format(arm));
            #The digital demodulation class.
        self.p_ad = AlignmentDither(ezca,
            ":SUS-ITM{0}_M0_OPTICALIGN_P_OFFSET".format(arm),
            ":",itm_G_pit);
            #Missing the offset channel.
                #":SUS-ITM{0}_M0_TEST_P_OFFSET".format(arm) ?
            #The alignment dither object for Pitch.
        self.y_ad = AlignmentDither(ezca,
            ":SUS-ITM{0}_M0_OPTICALIGN_Y_OFFSET".format(arm),
            ":",itm_G_yaw);
            #Missing the offset channel.
                #":SUS-ITM{0}_M0_TEST_Y_OFFSET".format(arm) ?
            #The alignment dither object for Yaw.
        #
        self.timer["max_wait"] = 60.0;
            #Try to align for up to 60 seconds.
            #TODO: In future use this timer as a maximum iteration time.
            #      After this maximum time it will jump to a side state, and 
            #      prompt the operator to confirm the ITM alignment is good
            #      enough.
    #
    
    def run(self):
        #Run the alignment steps.
        PD_pwr, Pit_demod, Yaw_demod = self.dig_demod.demod();
            #Collect the data, takes the default of 2 seconds.
        if PD_pwr < itm_pwr_min:
            log("Green power on ETM{0} Baffle PD 1, too low.".format(arm));
                #Tell the operator to manually align.
            #
            return("FIND_ETM_BFFL_PD_WITH_ITM");
                #Jump to manual alignment state.
        #
        self.p_ad.align_dither(Pit_demod, PD_pwr);
        self.y_ad.align_dither(Yaw_demod, PD_pwr);
            #Do one iteration of the pitch and yaw alignment dithering.
        #
        if self.timer["max_wait"]:
            return(True);
                #After 60 seconds move on.
                #TODO: In future use the error signal to determine the final
                #      point to stop.
        #
    #
#
#-------------------------------------------------------------------------------
#
class FIND_ETM_BFFL_PD_WITH_ITM(GuardState):
    #
    
    request = False; #Can't request.
    #index = ;
    
    def main(self):
        #
        self.dig_demod = DigitalDemodulation(ezca,
            ":AOS-ETM{0}_BAFFLEPD_1_POWER".format(arm),
            ":SUS-TMS{0}_M1_LOCK_P_INMON".format(arm),
            ":SUS-TMS{0}_M1_LOCK_Y_INMON".format(arm));
            #The digital demodulation class.
        #
        #Should the oscillator be turned off here?
        #
        self.timer["remind"] = 15.0;
            #Set up a timer for reminding the operator to align the ITM.
        #
    #
    
    def run(self):
        #
        PD_pwr, _ = self.dig_demod.demod();
            #Collect the data, takes the default of 2 seconds.
        if self.timer["remind"]:
            log("Green power on ETM{0} Baffle PD 1, too low. ".format(arm) + \
                "Please manually align ITM{0} to ETM{0} ".format(arm) + \
                "baffle 1 PD.");
                #Remind the operator to manually align.
            #
            self.timer["remind"] = 15.0;
                #Reset the timer.
        #
        if PD_pwr >= itm_pwr_min:
            log("Green power on ETM{0} Baffle PD 1 is sufficient.".format(arm));
            #
            return("ALIGN_ITM_TO_ETM_BFFL");
                #Jump back to the automatic alignment state.
                #Probably a return(True) would be sufficient here.
        #
        return(False);
            #Keep looping if power remains too low.
    #

#
#-------------------------------------------------------------------------------
#
class ITM_ALIGNED_TO_ETM_BFFL(GuardState):
    #Ensure that the ITM, green, is aligned to the ETM baffle.
    
    request = True; #You can request this state.
    #index = ;
    
    def main(self):
        #Turn off the TMS alignment oscillator.
        ezca.write(":ASC-ADS_PIT{0}_OSC_FREQ".format(tms_oscillator_number), 
                   0);
        ezca.write(":ASC-ADS_PIT{0}_OSC_CLKGAIN".format(tms_oscillator_number),
                   0);
        ezca.write(":ASC-ADS_LO_PIT_MTRX_{0}_{0}".format(tms_oscillator_number),
                   0);
            #Disable the TMS Pitch oscillator.
        #
        ezca.write(":ASC-ADS_YAW{0}_OSC_FREQ".format(tms_oscillator_number), 
                   0);
        ezca.write(":ASC-ADS_YAW{0}_OSC_CLKGAIN".format(tms_oscillator_number),
                   0);
        ezca.write(":ASC-ADS_LO_YAW_MTRX_{0}_{0}".format(tms_oscillator_number),
                   0);
            #Disable the TMS Yaw oscillator.
        #
        align_save(ITM);
            #Save the offset of the ITM.
        #
        ezca.switch(":SUS-ITM{0}_M0_TEST_P".format(arm), "OUTPUT", "OFF");
        ezca.switch(":SUS-ITM{0}_M0_TEST_Y".format(arm), "OUTPUT", "OFF");
            #Turn the outputs off.
        #
        self.timer["wait"] = 2.0;
            #Set a timer.
    #
    
    def run(self):
        #Move along after the timer expires.
        if self.timer["wait"]:
            return(True);
        #
    #
#
#-------------------------------------------------------------------------------
#
class ALIGN_ETM_TO_ITM_BFFL(GuardState):
    #
    
    request = False; #Transition state, don't request.
    #index = ;
    
    def main(self):
        #
        log("\"ALIGN_ETM_TO_ITM_BFFL\" currently does nothing.");
            #
        #
        self.timer["wait"] = 2.5;
            #Wait for 2.5 seconds.
    #
    
    def run(self):
        #
        if self.timer["wait"]:
            #Timer has elapsed.
            return(True);
        #
    #
#
#-------------------------------------------------------------------------------
#
class FIND_ITM_BFFL_PD_WITH_ETM(GuardState):
    #
    
    request = False; #Transition state, don't request.
    #index = ;
    
    def main(self):
        #
        log("\"FIND_ITM_BFFL_PD_WITH_ETM\" currently does nothing.");
            #
        #
        self.timer["wait"] = 2.5;
            #Wait for 2.5 seconds.
    #
    
    def run(self):
        #
        if self.timer["wait"]:
            #Timer has elapsed.
            return(True);
        #
    #
#
#-------------------------------------------------------------------------------
#
class ETM_ALIGNED_TO_ITM_BFFL(GuardState):
    #
    
    request = True; #You can request this.
    #index = ;
    
    def main(self):
        #
        log("\"ETM_ALIGNED_TO_ITM_BFFL\" currently does nothing.");
            #
        #
        self.timer["wait"] = 2.5;
            #Wait for 2.5 seconds.
    #
    
    def run(self):
        #
        if self.timer["wait"]:
            #Timer has elapsed.
            return(True);
        #
    #
#
#-------------------------------------------------------------------------------
#
class LOCK_GRN_ARM(GuardState):
    #Lock the arms to the green laser.
    #Done by the ALS Guardian.
    #This stage IS useful to have.
    
    request = False; #This is a transition state.
    #index = ;
    
    def main(self):
        #Locking procedure.
        #Ensure any offsets are removed from the TMS, ITM, and ETM optics.
            #
        #Ensure the TMS oscillations are turned off.
            #
        #als_loose.set_request("PDH_LOCKED");
            #Request the green PDH locked cavity.
        #
        self.timer["wait"] = 2;
            #A timer for 2 seconds.
        self.timer["timeout"] = 60;
            #A timeout timer for 60 seconds.
    #
    
    def run(self):
        #Periodically check the ALS is locked.
        if self.timer["wait"]:
            return True
            
            #2 seconds have elapsed.
            #if als_loose.arrived:
                #The ALS guardian is in the PDH_LOCKED state.
                #return(True);
            #else:
                #Reset the timer.
                #self.timer["wait"] = 2;
            #
        #
        if self.timer["timeout"]:
            log("ALS_{0}ARM guardian failed to reach ".format(arm) + \
                "\"PDH_LOCKED\" within 60 seconds (timeout).");
                #Inform the operator of what is going on.
            return("ARM_ALIGN_IDLE");
                #Reset the arm alignment, to try again.
        #
    #
#
#-------------------------------------------------------------------------------
#
class GRN_ARM_LOCKED(GuardState):
    #Ensure the green arms are locked.
    #A check on the ALS Guardian.
    
    request = True; #You would request this to align the arm cavities.
    #index = ;
    
    def run(self):
        return True
        
        #Check that the green arm cavities are locked.
        #if not als_loose.completed:
            #The PDH_LOCKED state is still running, wait for it.
            #return(False);
                #We are not ready to procede.
        #Perform other checks, for example stalled states.
        #elif als_loose.state != "PDH_LOCKED":
            #The state is not what it should be.
            #log("ALS_{0}ARM guardian is not in \"PDH_LOCKED\".".format(arm));
                #Warn the operator that the state is not matched to the intended
                #state.
            #
            #return("ARM_ALIGN_IDLE");
                #Restart alignment.
        #else:
            #return(True)
        #
    #
#
#-------------------------------------------------------------------------------
#
class GRN_IDLE(GuardState):
    #Wait with the green arms locked but perform NO checks.
    
    request = True; #You would request this to align the arm cavities.
    #index = ;
    
    def main(self):
        #Releases the ALS guardian.
        #als_loose.release();
            #Releases the ALS arm guardian from control by this guardian.
        #
        return(True);
            #Is always successful.
    #

#
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#                                    Edges
#-------------------------------------------------------------------------------
edges = [
         ("INIT", "ARM_ALIGN_IDLE"),
         ("ARM_ALIGN_IDLE", "ARM_ALIGN_RESET"),
         ("ARM_ALIGN_RESET", "MISALIGN_FOR_GRN_CAV"),
         #TMS alignment.
         ("MISALIGN_FOR_GRN_CAV", "ALIGN_GRN_TO_TMS"),
         ("ALIGN_GRN_TO_TMS", "GRN_ALIGNED_TO_TMS"),
         ("GRN_ALIGNED_TO_TMS", "MISALIGN_FOR_GRN_CAV"),
         #Arm cavity alignment.
         ("MISALIGN_FOR_GRN_CAV", "ALIGN_TMS_TO_ITM_BFFL"),
         ("ALIGN_TMS_TO_ITM_BFFL", "FIND_ITM_BFFL_PD_WITH_TMS"),
         ("FIND_ITM_BFFL_PD_WITH_TMS", "ALIGN_TMS_TO_ITM_BFFL"),
         ("ALIGN_TMS_TO_ITM_BFFL", "TMS_ALIGNED_TO_ITM_BFFL"),
         ("TMS_ALIGNED_TO_ITM_BFFL", "ALIGN_ITM_TO_ETM_BFFL"),
         ("ALIGN_ITM_TO_ETM_BFFL", "FIND_ETM_BFFL_PD_WITH_ITM"),
         ("FIND_ETM_BFFL_PD_WITH_ITM", "ALIGN_ITM_TO_ETM_BFFL"),
         ("ALIGN_ITM_TO_ETM_BFFL", "ITM_ALIGNED_TO_ETM_BFFL"),
         ("ITM_ALIGNED_TO_ETM_BFFL", "ALIGN_ETM_TO_ITM_BFFL"),
         ("ALIGN_ETM_TO_ITM_BFFL", "FIND_ITM_BFFL_PD_WITH_ETM"),
         ("FIND_ITM_BFFL_PD_WITH_ETM", "ALIGN_ETM_TO_ITM_BFFL"),
         ("ALIGN_ETM_TO_ITM_BFFL", "ETM_ALIGNED_TO_ITM_BFFL"),
         ("ETM_ALIGNED_TO_ITM_BFFL", "LOCK_GRN_ARM"),
         ("LOCK_GRN_ARM", "GRN_ARM_LOCKED"),
         #Allows managers/users to unlock the arm.
         ("GRN_ARM_LOCKED", "GRN_IDLE")
        ];
    #The edges defining the allowed guardian transitions.
    #Excludes automatic edges created by goto states.
#
#-------------------------------------------------------------------------------
#END.
