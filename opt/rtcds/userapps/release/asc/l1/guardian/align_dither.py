#!/usr/bin/env python
"""
Classes to do the digital demodulation, and a single pass of the optic alignment
 dithering. The digital demodulation class can handle an arbitrary number of 
modulation channels. The alignment dither class is designed to handle only one 
channel

Author: Nathan Holland.
Date: 2019-05-03
Contact: nathan.holland@ligo.org

Modified: 2019-05-03 (Created)
Modified: 2019-05-06 (Completed the initial coding)
"""
#-------------------------------------------------------------------------------
#Imports:
import numpy as np;
import time;
#-------------------------------------------------------------------------------
#Script variables:
__all__ = ["DigitalDemodulation", "AlignmentDither"];
    #I want these two class definitions only.
#
_demod_default_duration = 2.0;
    #Default duration for the digital demodulation.
_demod_default_pause = 0.05;
    #The default pause for the digital demodulation.
#
_dflt_gain = 1.0;
    #The default gain for the alignment dither class.
#
#-------------------------------------------------------------------------------
#Digital Demodulation:
class DigitalDemodulation(object):
    """
    A class to perform digital demodulation of one channel by one, or more, 
    other channels. It uses EPICS to collect the data and thus is restricted to 
    the EPICS Nyquist Frequency of 8 Hz as a maximum.
    """
    def __init__(self, ezca, demod_chnl, *mod_chnls, **kwargs):
        """
        Initialisation of the DigitalDemodulation class.
        
        Usage:
        obj = DigitalDemodulation(epics_axs, guard_tmr, trgt, *chan_list,
                                  **kwargs)
        obj       - The digital demodulation object.
        epics_axs - An ezca.Ezca object to access EPICS channels.
        trgt      - The target EPICS channel to demodulate.
        chan_list - Tuple of EPICS channels containing the modulations.
        kwargs    - Keyword arguments, dictionary. Uses:
                     duration - The duration, in seconds, to collect data for.
                                Default is 2.0.
                     pause    - The duration, in seconds, to pause for between 
                                sampling. Be cognisant of EPICS sampling rate of
                                 16 Hz, 0.0625 s.
                                Default is 0.05 s.
        """
        #
        if "duration" in kwargs.keys():
            self._duration = kwargs["duration"];
                #Accept the new duration.
        else:
            self._duration = _demod_default_duration;
                #Use the default.
        #
        if "pause" in kwargs.keys():
            self._pause = kwargs["pause"];
                #Accept the new pause.
        else:
            self._pause = _demod_default_pause;
                #Use the default.
        #
        self._channels = list(mod_chnls).insert(0, demod_chnl);
            #Create the list of the epics channels.
        #
        self._epics = ezca;
            #EPICS access.
        #
        self._size = (len(self._channels), int(self._duration / self._pause));
            #The size of the empty array.
    #
    
    def demod(self):
        """
        Collects the data, and performs the demodulation of the target channel 
        with the excitation channels. Returns a tuple of the mean of the target 
        channel followed by the target channel demodulated by each of the 
        excitation channels.
        
        Usage:
        mean_trgt, *mean_demods = dig_demod_obj.demod()
        mean_trgt     - Mean of the target channel, over the specified time 
                        period.
        mean_demods   - Tuple of the means, of the demodulations. This is in the
                         same order as provided when constructed. 
        dig_demod_obj - Instance of DigitalDemodulation class.
        """
        #
        data_array = np.zeros(self._size);
            #An empty array for all of the data.
        #
        for ii in range(size[1]):
            for jj in range(size[0]):
                data_array[jj, ii] = self._epics.read(self._channels[jj]);
                    #Read the data in channel by channel.
            #
            time.sleep(self._pause);
                #Pause to let EPICS update.
        #
        for ii in range(1, size[0]):
            data_array[ii] -= np.mean(data_array[ii]);
                #Remove the mean from the modulation channels.
        #
        for ii in range(1, size[0]):
            data_array[ii] = data_array[0] * data_array[ii];
                #Demodulation.
        #
        return(tuple(np.mean(data_array, axis = 1).tolist()));
            #Return the mean of the target, and demodulated channels.
    #
    
    def rb_channels(self):
        """
        Readback of the channels in this object.
        
        Usage:
        channels = dig_demod_obj.rb_channels()
        channels      - List of channels.
        dig_demod_obj - Instance of DigitalDemodulation class.
        """
        return(list(self._channels));
    #

#-------------------------------------------------------------------------------
#Alignment Dithering:
class AlignmentDither(object):
    """
    A class to perform one iteration of the optic alignment dithering for one 
    optic. It uses EPICS to provide this control. Error signals, and power
    normalisation need to be provided when running the iteration.
    """
    def __init__(self, ezca, ctrl_channel, offset_channel, gain = _dflt_gain):
        """
        Initialisation of the AlignmentDither class.
        
        Usage:
        obj = AlignmentDither(epics_axs, ctrl_chan, off_chan, gain)
        obj       - The AlignmentDither object.
        epics_axs - An ezca.Ezca object to provide EPICS access.
        ctrl_chan - The control channel to apply commands to.
        off_chan  - The offset channel to write the error signal to.
        gain      - A gain used for calculating the comand to apply.
        """
        self._epics = ezca;
            #EPICS channel access.
        self._ctrl_channel = ctrl_channel;
            #Control channel, to read back and feed back to.
        self._ofst_channel = offset_channel;
            #The offset channel, to save the error signal to.
        self._gain = gain;
            #The gain of the feedback.
    #

    def align_dither(self, error, norm):
        """
        Calculate, and apply the command signal to the specified channel. While 
        doing this save the error signal to the specified channel.
        
        Usage:
        align_dither_obj.align_dither(error, normalisation)
        align_dither_obj - Instance of AlignmentDither class.
        error            - The error signal, used to calculate the command.
        normalisation    - Normalisation, used to calcualte the command.
        """
        cmnd = self._epics.read(self._ctrl_channel) + self._gain * error / norm;
            #Calculate the command to apply to the specified channel.
        #
        self._epics.write(self._ctrl_channel, cmnd);
            #Apply the command to the specified channel.
        #
        self._epics.write(self._ofst_channel, error);
            #Save the error value.
    #
    
    def rb_gain(self):
        """
        Readback the gain saved in the this instance of AlignmentDither. Note 
        that this doesn't provide access to this.
        
        Usage:
        gain = align_dither_obj.rb_gain()
        gain             - The gain used while genrating the command.
        align_dither_obj - Instance of AlignmentDither class.
        """
        return(float(self._gain));
    #
    
    def rb_channels(self):
        """
        Readback the control and offset channels in this instance of 
        AlignmentDither. Note that this doesn't provide access to this.
        
        Usage:
        control_channel, offset_channel = align_dither_obj.rb_channels()
        control_channel  - The channel which the command is applied to.
        offset_channel   - The channel the error is saved to.
        align_dither_obj - Instance of AlignmentDither class.
        """
        return(str(self._ctrl_channel), str(self._ofst_channel));
    #
        
#-------------------------------------------------------------------------------
#END
