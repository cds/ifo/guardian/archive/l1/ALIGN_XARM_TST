#!/usr/bin/env python
"""
Rewrite of align_restore.py to be:
 > More Pythonic.
 > Importable as a function, which is better for guardian applications.

Should be maintained to be compatible with guardian. Funbdamentally this means 
that the script should be:
 > Able to be imported.
 > Continue to have the sole __all__ element \"align_restore\" performing the 
   function of this script for a single optic.
 > Not modify the path, guardian doesn't like this.
If in doubt ask.

Copied across from 
/opt/rtcds/userapps/trunk/sus/common/scripts/align_restore.py 
which was written by S. Aston in 2018.

Author: N. Holland.
Date (yyyy-mm-dd): 2019-05-02
Contact: nathan.holland@ligo.org

Modified: 2019-05-02 (Created)
Modified: 2019-05-03 (Finished preliminary coding)
Modified: 2019-05-21 (Minor debugging)
Modified: 2019-05-22 (Minor debugging)
Modified: 2019-05-23 (Debugging on test stand X2)
"""
#-------------------------------------------------------------------------------
#Imports:
import argparse;
import ezca;
import os;
import subprocess;
#
#-------------------------------------------------------------------------------
#EPICS configuration:
axs = ezca.Ezca();
    #EPICS channel access.
#
#-------------------------------------------------------------------------------
#Environment configuration (INCOMPATIBLE WITH GUARDIAN):
#import sys;
    #Allow the path to be changed (incompatible with guardian).
#sys.path.append('/ligo/cdscfg');
    #Find the location of /ligo/cdscfg/stdenv.py
#import stdenv as cds;
    #Import the standard CDS environment files.
#cds.INIT_ENV();
    #Initialise the standard CDS environment for the OS, and site.
#_userapps_dir = cds.USERAPPS;
    #Get the correct userapps directory.
#_ifo = cds.ifo;
    #Get the correct, lower case, IFO string.
#
#Alternative (LESS IDEAL):
if os.getenv("ifo") is not None:
    #If the environment variable 'ifo' is defined.
    _ifo = os.getenv("ifo").lower();
        #Lowered just to make sure.
elif os.getenv("IFO") is not None:
    #1st backup, if 'IFO' is defined.
    _ifo = os.getenv("IFO").lower();
else:
    #Hard, site specific, backup.
    _ifo = "l1";
#
if os.getenv("PRIVATE_USERAPPS") is not None:
    #Copied from /ligo/cdscfg/llo/stddir.py on 2019-05-22.
    #If there is a defined userapps directory.
    _userapps_dir = os.getenv("PRIVATE_USERAPPS");
else:
    #Copied from /ligo/cdscfg/llo/stddir.py on 2019-05-22.
    #Hard coded backup.
    _userapps_dir = os.path.join(os.path.sep, 'opt', 'rtcds', 'userapps',
                                 'release');
#
#-------------------------------------------------------------------------------
#Script Variables:
__all__ = ["align_restore"];
    #The functional version of this script.
#-------------------------------------------------------------------------------
#Functions:
def optic_details(optic):
    """
    Gets the filename, path and partial channel name for a specified optic.
    Hardcoded based upon the file this was copied from, dated 2018-05-10.
    
    Usage:
    path, filename, part_chan_name = optic_details(optic);
    path           - The path to the file storing the offset values.
    filename       - The filename storing the offset values.
    part_chan_name - A partial channel name, up to numbered mass.
    optic          - The name of the optic.
    """
    #
    namebit2 = optic[:2];
        #First 2 characters of the name.
    namebit3 = optic[:3];
        #First 3 characters of the name.
    #
    if namebit2 == "IM":
        pathname = _userapps_dir + '/sus/' + _ifo + '/burtfiles/im/';
            #Set path name - appropriately.
            #Copied.
    elif (namebit2 == "RM") or (namebit2 == "OM") or (namebit2 == "ZM"):
        pathname = _userapps_dir + '/sus/' + _ifo + '/burtfiles/htts/';
            #Set path name - appropriately.
            #Copied.
    else:
        pathname = _userapps_dir + '/sus/' + _ifo + '/burtfiles/' + \
                   optic.lower() + '/';
            #Set path name - appropriately.
            #Copied.
    if namebit3 == "OMC":
        pathname = _userapps_dir + '/sus/' + _ifo + '/burtfiles/' + \
                   optic.lower() + '/';
            #Set path name - appropriately.
            #Copied.
    #
    filename = optic + '_alignment_values.txt';
        #The filename to append the offset to.
    #
    comm_name = "SUS-" + optic;
        #The common part of the name.
    if (namebit3 == "ITM") or (namebit3 == "ETM"):
        part_chan_name = comm_name + '_M0_';
            #Use M0 for the TMs.
    else:
        part_chan_name = comm_name + '_M1_';
            #Use M1 for all other masses.
    #
    return(pathname, filename, part_chan_name);
#
def channel_deflection(path, filename, part_chan_name):
    """
    Reads the optic appropriate deflections, from provided path and file, and 
    then returns a dictionary of full EPICS channel name, and deflection.
    
    Usage:
    deflect_dict = channel_deflection(path, filename, part_chan_name)
    deflect_dict   - A dictionary mapping EPICS channels to offsets.
    path           - Path to the file.
    filename       - File containing the offsets to restore.
    part_chan_name - Partial EPICS channel name.
    """
    #
    namebit3 = part_chan_name[4:7];
        #First 3 characters of the optic name.
    #
    last_line = subprocess.check_output(["tail","-n 1", path + filename]);
        #Use tail to read the second last line of the file.
        #This IS the best way to do this.
    contents = last_line.split();
        #Split on default, space, delimiter.
        #Pitch will be the penultimate number - if present.
        #Yaw will be the final number, if present.
    #
    pit_chan = "OPTICALIGN_P_OFFSET";
        #Channel name completion for pitch.
    yaw_chan = "OPTICALIGN_Y_OFFSET";
        #Channel name completion for yaw.
    #
    if namebit3 == "OFI":
        #OFI optic only has yaw.
        deflect_dict = {part_chan_name + yaw_chan : float(contents[-1])};
            #Assign the channel name, value dictionary.
    else:
        deflect_dict = {part_chan_name + pit_chan : float(contents[-2]),
                        part_chan_name + yaw_chan : float(contents[-1])};
            #Assign the channel name, value dictionary.
    #
    return(deflect_dict);
#
def write_offset(deflect_dict):
    """
    Writes the offsets, as provided, in the deflection dictionary. Designed to 
    take the deflection dictionary provided by channel_deflection.
    
    Usage:
    write_offset(deflect_dict)
    deflect_dict - A dictionary mapping EPICS channels to offsets.
    """
    for channel_name, offset in deflect_dict.items():
        axs.write(channel_name, offset);
            #Write the value/s.
    #
#
#-------------------------------------------------------------------------------
#MAIN:
def align_restore(optic):
    """
    Restores the yaw, and often pitch, of the requested optic, as they were 
    saved to file. Only yaw is restored for the OFI. Reads the last, non-empty, 
    line of the file and restores that value.
    
    Usage:
    align_restore(optic)
    optic - The optic to restore the alignment of.
    """
    path, filename, part_chan_name = optic_details(optic);
        #Get the path, filename, and partial EPICS channel name.
    deflect_dict = channel_deflection(path, filename, part_chan_name);
        #Get a dictionary of EPICS channel name and offset values.
        #EPICS channel names are the keys.
        #Offsets are the items.
    write_offset(deflect_dict);
        #Write the offset/s with EPICS.
#
#-------------------------------------------------------------------------------
#Argparse:
_prog_descr = "Restores the current offset, pitch and yaw (generally) on ";
_prog_descr += "the requested optic. Only yaw can be restored on the OFI.";
    #The description of this program, briefly.
parser = argparse.ArgumentParser(description = _prog_descr);
    #The argument parser, with a program description.
#
optic_help = "Please provide one, or more optic names eg. ETMY, SR3.";
    #The description of the optic argument.
parser.add_argument("optic", nargs = '+', type = str, help = optic_help);
    #Add the optic argument, all arguments are collected into a list.
#
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parser.parse_args();
            #Parse the arguments given.
    #
    for optik in args.optic:
        align_restore(optik);
            #Save all of the optics provided.
    #
else:
    pass;
#
#-------------------------------------------------------------------------------
#END.

